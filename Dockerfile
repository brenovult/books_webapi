FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY . ./
ENTRYPOINT eval dotnet /app/Books.WebAPI.dll