using System.Collections.Generic;
using Books.Domain.Models;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Linq;
using Dapper.Contrib.Extensions;

namespace Books.Adapter.Repositories
{
    public class BooksRepository
    {
        private IConfiguration _conf;

        public BooksRepository(IConfiguration config)
        {
            _conf = config;
        }


        public BooksDTO GetBooks(int id)
        {
            using (MySqlConnection connection = new MySqlConnection( 
                _conf.GetValue<string>("MySqlConnectionString")))
            {
                return connection.Get<BooksDTO>(id);
            }
        }

        public long InsertBooks(BooksDTO book)
        {
            using (MySqlConnection connection = new MySqlConnection( 
                _conf.GetValue<string>("MySqlConnectionString")))
            {
                return connection.Insert<BooksDTO>(book);
            }
        }

        public bool Delete(BooksDTO book)
        {
            using (MySqlConnection connection = new MySqlConnection( 
                _conf.GetValue<string>("MySqlConnectionString")))
            {
                return connection.Delete<BooksDTO>(book);
            }
        }

        public bool Put(BooksDTO book)
        {
            using (MySqlConnection connection = new MySqlConnection( 
                _conf.GetValue<string>("MySqlConnectionString")))
            {
                return connection.Update<BooksDTO>(book);
            }
        }

        public IEnumerable<BooksDTO> GetAllBooks()
        {
            using (MySqlConnection connection = new MySqlConnection( 
                _conf.GetValue<string>("MySqlConnectionString")))
            {
                //somente para testar select
                return connection.Query<BooksDTO>("SELECT id, title FROM Books").ToList(); 
            }
        }

    }
}