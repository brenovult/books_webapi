using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration.AzureKeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.AspNetCore;

namespace Books.WebAPI
{
 public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                    .ConfigureAppConfiguration((context, config) =>
                    {
                        config.AddAzureKeyVault(
                            $"https://{Environment.GetEnvironmentVariable("AZURE_VAULT")}.vault.azure.net/",
                            Environment.GetEnvironmentVariable("AZURE_CLIENTID"),
                            Environment.GetEnvironmentVariable("AZURE_CLIENTSECRET")
                        );

                    })
                    .UseStartup<Startup>();

    }
}
