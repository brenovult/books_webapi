using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using Books.Domain.Models;
using Books.Adapter.Repositories;

namespace Books.WebAPI.BooksController
{
    [ApiController]
    [Route("api/[controller]")]

    public class BooksController : ControllerBase
    {
        private IConfiguration _config;
        public BooksController([FromServices]IConfiguration Config)
        {
            this._config = Config;
        }

        [HttpGet("{id}")]
        public BooksDTO Get(int id){
            return new BooksRepository(this._config).GetBooks(id);
        }
        [HttpPost]
        public long Post([FromBody]BooksDTO book){
           return new BooksRepository(this._config).InsertBooks(book);
        }

        [HttpGet]
        public IEnumerable<BooksDTO> Get(){
            return new BooksRepository(this._config).GetAllBooks();
        }
        
        [HttpPut]
        public ActionResult Put([FromBody]BooksDTO book){
            var result =  new BooksRepository(this._config).Put(book);
            if(result == false)
                return BadRequest("O livro não foi encontrado!");
            return Ok();
        }


        [HttpDelete]
        public ActionResult Delete([FromBody]BooksDTO book){
            var result = new BooksRepository(this._config).Delete(book);
            if(result == false)
                return NotFound();
            return NoContent();
        }


        

    }

}