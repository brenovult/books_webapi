using Dapper.Contrib.Extensions;

namespace Books.Domain.Models
{
    [Table("Books")]
    public class BooksDTO
    {
        [Dapper.Contrib.Extensions.Key]
        public int id{ get; set;}
        public string title {get ; set;}

    }
}